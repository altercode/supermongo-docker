## USAGE

1. Start the docker container exposing the RDP port (33890) with `docker run -ti --hostname supermongo -p 33890:3389 registry.gitlab.com/altercode/supermongo-docker` as interactive or `docker run -d --name uxrdp --hostname supermongo -p 33890:3389 registry.gitlab.com/altercode/supermongo-docker` as detached container
2. Open Microsoft Remote Desktop Client app (for windows and mac) or something like Remmina on linux (https://remmina.org/) 
3. Connect to `localhost:33890`
4. Enter with `ubuntu` as _user_ and _password_
## About XRDP
See https://github.com/danielguerra69/ubuntu-xrdp