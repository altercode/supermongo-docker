FROM danielguerra/ubuntu-xrdp:20.04

RUN apt update -y
RUN apt install -y emacs xterm nano

#COPY sm2_4_34 /sm_sources
ADD ./bins/sm /usr/local/bin/sm
ADD ./sm2_4_34/filecap /usr/local/lib/sm/filecap
ADD ./sm2_4_34/fonts.bin /usr/local/lib/sm/fonts.bin

ADD ./sm2_4_34/graphcap /usr/local/lib/sm/graphcap
ADD ./sm2_4_34/termcap /usr/local/lib/sm/termcap
ADD ./sm2_4_34/macro /usr/local/lib/sm/macro

ADD sm2_4_34/help /usr/lib/sm/help

COPY ./configs /sm-configs
ADD ./docker-supermongo-entrypoint.sh /usr/bin/docker-supermongo-entrypoint.sh

RUN chmod +x /usr/bin/docker-supermongo-entrypoint.sh
RUN chmod +x /usr/local/bin/sm
RUN chmod +x /usr/local/lib/sm/filecap
RUN chmod +x /usr/local/lib/sm/graphcap
RUN chmod +x /usr/local/lib/sm/termcap

VOLUME ["/etc/ssh","/home"]
EXPOSE 3389 22 9001
ENTRYPOINT ["/usr/bin/docker-entrypoint.sh","/usr/bin/docker-supermongo-entrypoint.sh"]

CMD ["supervisord"]